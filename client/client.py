#Basic architecture(idea of a class,__init__ function etc taken from https://docs.python.org/3/howto/sockets.html
class client:

	def __init__(self,sock=None):

		if sock is None:
			self.sock = socket.socket(
							socket.AF_INET, socket.SOCK_STREAM)
		else:
			self.sock = sock

	def setUsername(self,username):
		self.username = username

	def recvBytes(self):
		buf = ""
		while ' ' not in buf:
			buf += self.sock.recv(1).decode('utf-8')
		n = int(buf)
		msg = self.sock.recv(n).decode('utf-8')
		return msg

	def sendBytes(self,msg):
		n = len(msg.encode('utf-8'))
		n_string = str(n) + ' '
		self.sock.send(n_string.encode('utf-8'))
		self.sock.send(msg.encode('utf-8'))
