import socket 
import sys
from client import *
import select
import getpass

s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)

SERVER_IP = str(sys.argv[1])
SERVER_PORT = int(sys.argv[2])


cli = client(s)
cli.sock.connect((SERVER_IP,SERVER_PORT))


#-------------SIGNUP---------------------
def signup():
	cli.sendBytes('signup')

	#Username
	message = cli.recvBytes()
	print (message)
	username = input()
	cli.sendBytes(username)
	message = cli.recvBytes()
	print (message)

	while message.split('|')[-1]== "Username already taken. Enter another username:":
		username = input()
		cli.sendBytes(username)
		message = cli.recvBytes()
		print (message)

	password1 = getpass.getpass('')
	cli.sendBytes(password1)
	message = cli.recvBytes()
	print (message)

	password2 = getpass.getpass('')
	cli.sendBytes(password2)
	message = cli.recvBytes()
	print (message)

	while message.split('|')[-1]== 'Entered passwords do not match!':
		message = cli.recvBytes()
		print (message)

		password1 = getpass.getpass()
		cli.sendBytes(password1)
		message = cli.recvBytes()
		print (message)

		password2 = getpass.getpass()
		cli.sendBytes(password2)
		message = cli.recvBytes()
		print (message)


#---------------------------LOGIN----------------------------------
def login():
	# TODO: not logging out after keyboard interrupt here.
	cli.sendBytes('login')
	message = cli.recvBytes()
	while message.split('|')[-1]!="Login Successful. You are now logged in.":
		print (message)
		if message.split('|')[-1]!= "Enter Username:" and message.split('|')[-1]!="Enter Password:":
			#TODO: change to login
			cli.sendBytes(input())

		elif message.split('|')[-1]== "Enter Username:":
			username = input()
			cli.sendBytes(username)

		elif message.split('|')[-1]== "Enter Password:":
			Password = getpass.getpass('')
			cli.sendBytes(Password)

		message = cli.recvBytes()

	print(message)

# ------------------------------------------------------------------


while True:

	try:
		sockets_list = [sys.stdin, s]
		read_sockets,write_socket, error_socket = select.select(sockets_list,[],[])
		for socks in read_sockets:
			if socks == s:
				data = cli.recvBytes()
				if not data:
					#TODO: disconnected from chat server
					pass
				else:
					#print the data
					print (data)

			else:

				# ------------TODO--------------
				data = input()
				if data == 'signup':
					signup()
				elif data == 'login':
					login()
				else:
					cli.sendBytes(data)
					
	except KeyboardInterrupt:

		print("Keyboard Interrupt")
		cli.sendBytes("logout")
		exit(1)

cli.sock.close()
