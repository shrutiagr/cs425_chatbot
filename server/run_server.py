import socket
import sys
import csv
from threading import *
from server_protocol import *
from user import user

# user_dict = {}
# online_list = []

class clientThread(Thread):

	def __init__(self,cli_sock):
		Thread.__init__(self)
		self.cli_sock= cli_sock

	def run(self):
		handleClient(self.cli_sock)


#---------------------------------------------------------------------------------------------

def handleClient(cli_sock):
	sendBytes(cli_sock,"Welcome to the chatroom!")
	#TODO: display options
	sendBytes(cli_sock,"Login or SignUp to continue")

	cli_obj = loginOrSignup(cli_sock)

	while cli_obj == None:
		sendBytes(cli_sock,'You must Login or SignUp to continue!')
		cli_obj = loginOrSignup(cli_sock)
		# cli_sock.close()
	# else:
		# msg = 'Login Successful\n'
		# sendBytes(cli_obj.sock,msg)

	while True:
		try:
			data = recvBytes(cli_obj.sock)
			# print ("data",data)
			processRecvData(cli_obj,data)
		except KeyboardInterrupt:
			continue
	print (recvBytes(cli_obj.sock))

SERVER_IP = str(sys.argv[1])
SERVER_PORT = int(sys.argv[2])
# Basic socket handling is taken from the examples snippets here: https://docs.python.org/3/howto/sockets.html

serv_sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
serv_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

serv_sock.bind((SERVER_IP,SERVER_PORT))

serv_sock.listen(10)
print("Server running on",SERVER_IP,SERVER_PORT)

initializeUserDict()


while True:
	(cli_sock, addr) = serv_sock.accept()
	# print addr
	print("Connection from",addr)
	client_thread = clientThread(cli_sock)
	try:
		client_thread.start()
	except:
		pass

cli_sock.close()
