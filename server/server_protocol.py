import csv 
import datetime
import time
import socket
from bcrypt import hashpw,gensalt,checkpw
from user import user

user_dict = {}
online_list = []
block_times = {}

def logout(username):
	user_obj = user_dict[username]
	user_obj.last_logout = time.time()
	# print(user_obj.last_logout)
	#have locks here as it is a global variable 
	try:
		online_list.remove(username)
		print(username,"logged out")
		print("online list after logout",online_list)
	except ValueError:
		print("{} wasn't there in the online list! ".format(user_obj.username))
		pass
	user_obj.online = False

def sendBytes(sock,msg):

	msg =str(time.strftime("%Y-%m-%d %H:%M:%S"))+' |'+msg
	n = len(msg.encode('utf-8'))
	n_string = str(n) + ' '
	try:
		sock.send(n_string.encode('utf-8'))
		sock.send(msg.encode('utf-8'))
		print ("server sending",msg)
	except:
		print("client closed")

def recvBytes(sock):
	buf = ""
	while ' ' not in buf:
		try:
			buf += sock.recv(1).decode('utf-8')
		except:
			break
	n = int(buf)
	try:
		msg = sock.recv(n)
	except:
		print("client clsoed")
	return msg.decode('utf-8')

def verifyUser(username,text_password):

	#Adding encryted passwords
	file = './server/id_passwords.csv'

	with open(file) as csvfile:
		reader = csv.reader(csvfile, delimiter=',')
		for row in reader:
			hashed_password = row[1]
			# print(hashed_password)
			if row[0] == username and checkpw(text_password.encode('utf-8'),hashed_password.encode('utf-8')):
				return True
	return False
	close(file)

def addUser(username,text_password):
	hashed_password = hashpw(text_password.encode('utf-8'),gensalt())
	hashed_password = hashed_password.decode('utf-8')
	file = './server/id_passwords.csv'
	with open(file,'a') as csvfile:
		writer = csv.writer(csvfile, delimiter=',')
		writer.writerow([username,hashed_password])
	# close(file)

def tripleLogin(cli_sock):

	login_attempts = 3
	try:
		block_time = block_times[cli_sock.getsockname()[0]]
	except:
		block_time = 0

	#TODO : How to block ?
	if time.time()-block_time > 60:
		# print (time.time()-block_time)
		while login_attempts > 0:
			cli_obj = login(cli_sock)
			if cli_obj is not None:
				sendBytes(cli_sock,"Login Successful. You are now logged in.")
				return cli_obj
			else:
				login_attempts = login_attempts-1
				sendBytes(cli_sock," ".join(["Incorrect username and/or password.",str(login_attempts),"attempts remaining. Press Enter to continue."]))
				#TODO: Should not recieving bytes here
				recvBytes(cli_sock)


		if login_attempts == 0:
			sendBytes(cli_sock,"Three Incorrect Attempts. You have been blocked for 60 seconds.")
			block_times[cli_sock.getsockname()[0]] = time.time()
			login_attempts = 3
			recvBytes(cli_sock)

	else:
		sendBytes(cli_sock,"You are blocked, retry after "+str(60-int(time.time()-block_time))+" seconds.")
		recvBytes(cli_sock)

def loginOrSignup(cli_sock):
	data_arr = recvBytes(cli_sock).split(" ")


	if(data_arr[0] == 'login'):
		cli_obj = tripleLogin(cli_sock)

		#--Sending Pending messages---
		# TODO: check for None object
		try:
			print (cli_obj.username,"logged in")
			print ("pending message buffer for "+cli_obj.username ,cli_obj.message_buffer)
			for msg in cli_obj.message_buffer:
				#TODO: print number of old messages 
				sendBytes(cli_obj.sock,"<old pm>"+msg)
			cli_obj.message_buffer=[]
		except:
			pass
		#-----------------------------
		return cli_obj

	elif (data_arr[0] == 'signup'):
		#Assume no pending messages if just signed up.
		return signUp(cli_sock)
	else:
		sendBytes(cli_sock,'Not a valid command!')
		return None

def signUp(cli_sock):
	sendBytes(cli_sock,"Enter a username:")

	# Check if username is already taken
	username = recvBytes(cli_sock)
	while username in user_dict.keys():
		sendBytes(cli_sock,"Username already taken. Enter another username:")
		username = recvBytes(cli_sock)	

	sendBytes(cli_sock,"Enter a password:")
	password = recvBytes(cli_sock)
	sendBytes(cli_sock,"Re enter password:")
	password2 = recvBytes(cli_sock)

	#Check if entered passwords match
	while password!=password2:
		sendBytes(cli_sock,"Entered passwords do not match!")
		sendBytes(cli_sock,"Enter a password:")
		password = recvBytes(cli_sock)
		sendBytes(cli_sock,"Re enter password:")
		password2 = recvBytes(cli_sock)

	addUser(username,password)
	sendBytes(cli_sock,'Sign Up Successful.')
	user_dict[username]=None
	print ("user dict after signup",user_dict)
	return getUserOnline(username,cli_sock)

def getUserOnline(username,cli_sock):
	online_list.append(username)
	print ("get User online, online list:",online_list)

	if user_dict[username] is None:
		cli_obj = user(cli_sock)
		cli_obj.setUsername(username)
		cli_obj.online =True
		user_dict[username] = cli_obj
		
		return cli_obj
	else:
		cli_obj = user_dict[username]
		cli_obj.sock = cli_sock
		user_dict[username].online = True
		return user_dict[username]	


def login(cli_sock):

	sendBytes(cli_sock,"Enter Username:")
	username = recvBytes(cli_sock)
	sendBytes(cli_sock,"Enter Password:")
	password = recvBytes(cli_sock)

	#prevent multiple logins.
	if(verifyUser(username,password)):
		if username in online_list:
			print ("multiple logins",online_list)
			sendBytes(cli_sock,"Already logged into another session. Multiple logins not allowed.")
			return

		return getUserOnline(username,cli_sock)
	else:
		return None

def initializeUserDict():
	file = './server/id_passwords.csv'
	with open(file) as csvfile:
		reader = csv.reader(csvfile, delimiter=',')
		for row in reader:
			#TODO: fix hack
			if len(row)>0:
				user_dict[row[0]]= None

#-------------------------------------------------------------------------------------------
## TODO: Move to another module

def processRecvData(cli_obj,data):
	data_arr = data.split(' ')
	if data_arr[0]=='broadcast':
		#TODO: how to handle online list
		broadcast(cli_obj.username,cli_obj.username+":"+" ".join(data_arr[1:]))
	elif data_arr[0]=='message':
		reciever = data_arr[1]
		privateMessage(cli_obj.username,reciever," ".join(data_arr[2:]))
	elif data_arr[0]=='whoElse':
		whoElse(cli_obj.username)
	elif data_arr[0]=='whoLastHour':
		whoLastHour(cli_obj.username)
	elif data_arr[0]=='logout':
		logout(cli_obj.username)
	elif data_arr[0]=='block':
		cli_obj.block(data_arr[1])
		print ("printing block list",cli_obj.block_list)
	elif data_arr[0]=='unblock':
		cli_obj.unblock(data_arr[1])
	elif ' '.join(data_arr[0:4])=='start private chat with':
		privateChat(cli_obj,data_arr[4])
	else:
		sendBytes(cli_obj.sock,"Invalid option")

def privateChat(cli_obj,reciever):

	cli_sock = cli_obj.sock
	data = recvBytes(cli_sock)
	while data!= 'end private chat':
		processRecvData(cli_obj,' '.join(['message',reciever,data]))
		data = recvBytes(cli_sock)

#TODO: bug - send even logout if logsout in the middle
#TODO: bug - keyboard interrupt from somewhere else
def privateMessage(sender,reciever,msg):
	msg = sender+ ': ' +msg
	print ("private msg online list:",online_list)
	if reciever in online_list:
		if sender in user_dict[reciever].block_list:
			sendBytes(user_dict[sender].sock,"You have been blocked by "+reciever)
			return
		sendBytes(user_dict[reciever].sock,msg)
	# TODO: Handle!
	else:
		# reciever exists
		if reciever in user_dict.keys():
			try:
				if sender in user_dict[reciever].block_list:
					sendBytes(user_dict[sender].sock,"You have been blocked by "+reciever)
					return

				user_dict[reciever].message_buffer.append("<"+str(time.strftime("%Y-%m-%d %H:%M:%S"))+">"+msg)
				print (user_dict[reciever].message_buffer)
			except:
				user_dict[reciever] = user()
				user_dict[reciever].setUsername(reciever)
				user_dict[reciever].message_buffer.append("<"+str(time.strftime("%Y-%m-%d %H:%M:%S"))+">"+msg)
		else:
			sendBytes(user_dict[sender].sock,"This user doesn't exist")

def broadcast(sender,msg): # function for broadcasting
	print ("broadcast online list:",online_list)

	for client in online_list:
		if client != sender:
			print ("broadcasting to:",client,user_dict[client].sock,msg)
			sendBytes(user_dict[client].sock,msg)

def whoLastHour(sender):
	
	not_online = [x for x in user_dict.keys() if x not in online_list]
	last_hour_list = []

	for username in not_online:

		user_obj = user_dict[username]
		if user_obj is not None and user_obj.last_logout !=None :

			diff_time = user_obj.last_logout -time.time()
			print (diff_time)
			if diff_time <3600:
				last_hour_list.append(username)

	sendBytes(user_dict[sender].sock,"System:"+','.join(last_hour_list+ online_list))
		
def whoElse(sender):
	list_to_send = [x for x in online_list if x!=sender]
	if len(list_to_send) ==0:
		sendBytes(user_dict[sender].sock,"System: There is no one else online!")
	else:
		sendBytes(user_dict[sender].sock,"System:"+','.join(list_to_send))

