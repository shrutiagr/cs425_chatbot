import csv 

def sendBytes(sock,msg):
	n = len(msg.encode('utf-8'))
	n_string = str(n) + ' '
	try:
		sock.send(n_string)
		sock.send(msg)
		print ("server sending",msg)
	except:
		print("client closed the connection")

def recvBytes(sock):
	buf = ""
	while ' ' not in buf:
		try:
			buf += sock.recv(1)
		except:
			print("The client closed the connection")
	n = int(buf)
	try:
		msg = sock.recv(n)
	except:
		print("The client closed the connection")

	return msg

def verifyUser(username,password):

	file = './server/id_passwords.csv'

	with open(file) as csvfile:
		reader = csv.reader(csvfile, delimiter=',')
		for row in reader:
			if row[0] == username and row[1] == password:
				return True
	return False
	close(file)
	
def login(cli_sock):
	data_arr = recvBytes(cli_sock).split(" ")
	if(data_arr[0] == 'login'):
		username = data_arr[1]
		password = data_arr[2]

		if(verifyUser(username,password)):

			#Update list of Online Users
			online_list.append(username)

			if user_dict[username] is None:
				cli_obj = user(cli_sock)
				cli_obj.setUsername(username)
				user_dict[username] = cli_obj
				return cli_obj
			else:

				#TODO update cli_obj in dict
				cli_obj = user_dict[username]
				for msg in cli_obj.message_buffer:
					sendBytes(cli_obj.sock,"<old pm>"+msg[1]+":"+msg[2])
				return user_dict[username]
		else:
			return None

	else:
		sendBytes(cli_sock,'Not a valid command!')
		return None

def initializeUserDict():
	file = './server/id_passwords.csv'
	with open(file) as csvfile:
		reader = csv.reader(csvfile, delimiter=',')
		for row in reader:
			user_dict[row[0]]= None

#-------------------------------------------------------------------------------------------
## TODO: Move to another module

def processRecvData(cli_obj,data):
	data_arr = data.split(' ')
	if data_arr[0]=='broadcast':
		#TODO: how to handle online list
		broadcast(cli_obj.username,cli_obj.username+":"+" ".join(data_arr[1:]))
	if data_arr[0]=='message':
		reciever = data_arr[1]
		privateMessage(cli_obj.username,reciever," ".join(data_arr[2:]))

def privateMessage(sender,reciever,msg):
	msg = sender+ ': ' +msg
	if reciever in online_list:
		sendBytes(user_dict[reciever].sock,msg)

	# TODO: Handle!
	else:
		if reciever in user_dict.keys():
			try:
				user_obj.message_buffer.append([self.username,msg])
			except:
				user_dict[reciever] = user()
				user_dict[reciever].setUsername(send_user)
				user_dict[reciever].message_buffer.append([self.username,msg])

def broadcast(sender,msg): # function for broadcasting
	for client in online_list:
		if client != sender:
			print (client,user_dict[client].sock,msg)
			sendBytes(user_dict[client].sock,msg)
#---------------------------------------------------------------------------------------------
