#Basic architecture(idea of a class,__init__ function etc taken from https://docs.python.org/3/howto/sockets.html
import datetime
from util import *
class user:

	def __init__(self,sock=None):

		self.sock = sock
		self.block_list = []
		self.message_buffer = []
		self.online = False
		self.last_logout = None

	def setUsername(self,username):
		self.username = username

	def block(self,user_to_block):
		
		self.block_list.append(user_to_block)

	def unblock(self,user_to_unblock):

		try:
			self.block_list.remove(user_to_unblock)
		except ValueError:
			sendBytes(self.sock,"System"+"{} is not blocked by you".format(user_to_unblock))
